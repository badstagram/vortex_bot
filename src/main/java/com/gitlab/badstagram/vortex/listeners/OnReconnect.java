package com.gitlab.badstagram.vortex.listeners;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;

import com.gitlab.badstagram.vortex.utils.Config;

import net.dv8tion.jda.api.events.ReconnectedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class OnReconnect extends ListenerAdapter {
    @Override
    public void onReconnect(@Nonnull ReconnectedEvent event) {
        WebhookEmbedBuilder builder = new WebhookEmbedBuilder();
        builder.setTitle(new WebhookEmbed.EmbedTitle("Shard Reconnected!", null));
        builder.setColor(0x00FF00);
        builder.addField(new WebhookEmbed.EmbedField(false, "Shard ID", "0"));

        WebhookMessageBuilder webhookMessageBuilder = new WebhookMessageBuilder();
        webhookMessageBuilder.addEmbeds(builder.build());
        WebhookClient client = WebhookClient.withUrl(Config.getConfig().getString("bot-status-logging-webhook-url"));
        client.send(webhookMessageBuilder.build()).thenAccept(ignored -> client.close());
    }
}

