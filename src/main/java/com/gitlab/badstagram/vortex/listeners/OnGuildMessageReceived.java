package com.gitlab.badstagram.vortex.listeners;

import com.gitlab.badstagram.vortex.CommandManager;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

public class OnGuildMessageReceived extends ListenerAdapter {
    private final CommandManager manager = new CommandManager();

    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent event) {
        Message message = event.getMessage();
        if (event.getAuthor().isBot() || message.isWebhookMessage()) {
            return;
        }

        if (Vortex.getAfkUsers().contains(event.getMember().getIdLong())) {
            message.getChannel().sendMessage("Welcome back. I have removed your AFK status.").queue();
            message.getMember().modifyNickname(null).queue();
            Vortex.getAfkUsers().remove(event.getMember().getIdLong());
        }




        if (message.getContentRaw().equalsIgnoreCase("Up up down down left right left right B A start")) {
            event.getChannel().sendMessage("Developer privileges granted!").queue();
        }

        manager.handleCommand(event);


        try {
            File file = new File("./copypastas.json");
            String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
            JSONObject object = new JSONObject(content);
            JSONArray copypastas = object.getJSONArray("copypastas");

            for (Object obj : copypastas) {
                String copypasta = "";
                if (obj instanceof String) {
                    copypasta = (String) obj;
                }

                if (message.getContentRaw().toLowerCase().contains(copypasta.replaceAll("\\r", "").toLowerCase())) {
                    event.getChannel().sendMessage(event.getAuthor().getAsMention() + ", Don't post copypastas.").queue();
                    message.delete().reason("[Anti-spam] Copypasta").queue();
                }
            }
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }

        Pattern tokenPattern = Pattern.compile("([a-zA-Z0-9]+)\\.([a-zA-Z0-9\\-_]+)\\.([a-zA-Z0-9\\-_]+)");

        if (event.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_MANAGE)) {
            if (tokenPattern.matcher(message.getContentRaw()).matches()) {
                message.delete().reason("Token Detected").queue();
                event.getChannel().sendMessageFormat("%s, I have deleted your message because it appears you have leaked a token. You should (Reset your bots token)[https://discord.com/developers/applications] / Change your password", event.getAuthor().getAsMention()).queue();
            }

        } else {
            event.getChannel().sendMessageFormat("%s, I have detected a token in your message however I was unable to delete it. You should Reset your bots token / Change your password", event.getAuthor().getAsMention()).queue();
        }
    }
}

