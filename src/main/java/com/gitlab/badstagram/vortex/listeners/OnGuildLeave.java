package com.gitlab.badstagram.vortex.listeners;

import com.gitlab.badstagram.vortex.main.Vortex;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class OnGuildLeave extends ListenerAdapter {

    @Override
    public void onGuildLeave(@Nonnull GuildLeaveEvent event) {
        Vortex.getApi().getPresence().setActivity(Activity.watching(String.format("%d guilds", Vortex.getApi().getGuilds().size())));
    }
}

