package com.gitlab.badstagram.vortex.listeners;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import com.gitlab.badstagram.vortex.CommandManager;
import com.gitlab.badstagram.vortex.utils.Config;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.EnumSet;

public class OnReady extends ListenerAdapter {

    @Override
    public void onReady(@Nonnull ReadyEvent event) {
        Logger logger = LoggerFactory.getLogger(this.getClass());
        JDA jda = event.getJDA();
        logger.info("{} is online!", jda.getSelfUser().getAsTag());
        logger.info("{} commands loaded!", CommandManager.getAsList().size());

        jda.getPresence().setActivity(Activity.watching(String.format("%d guilds", jda.getGuilds().size())));
        jda.getPresence().setStatus(OnlineStatus.ONLINE);


        WebhookEmbedBuilder builder = new WebhookEmbedBuilder();
        builder.setTitle(new WebhookEmbed.EmbedTitle("Bot Online!", null));
        builder.setColor(0x00FF00);

        WebhookMessageBuilder webhookMessageBuilder = new WebhookMessageBuilder();
        webhookMessageBuilder.addEmbeds(builder.build());
        WebhookClient client = WebhookClient.withUrl(Config.getConfig().getString("statusLog"));
        client.send(webhookMessageBuilder.build()).thenAccept(ignored -> client.close());
        EnumSet<GatewayIntent> intents = event.getJDA().getGatewayIntents();


        if (intents.contains(GatewayIntent.GUILD_MEMBERS) || intents.contains(GatewayIntent.GUILD_PRESENCES)) {
            final int guildSize = event.getJDA().getGuilds().size();
            System.out.println("*****************************************************************************");
            System.out.println("WARNING: Privileged GatewayIntents used. This will limit Vortex to 100 guilds.");
            System.out.println(String.format("Current guild count %d, guilds until cap %d", guildSize, 100 - guildSize));
            System.out.println("****************************************************************************");
        }

    }


}

