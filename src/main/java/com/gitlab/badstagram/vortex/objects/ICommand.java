package com.gitlab.badstagram.vortex.objects;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

import com.gitlab.badstagram.vortex.Constants;

public interface ICommand {

    /**
     * Handles a command.
     * @param args       The arguments that was passed into the command.
     * @param event      The {@link GuildMessageReceivedEvent} that triggered the event.
     * @param jda        The {@link JDA} instance
     * @param channel    The {@link TextChannel} the command was sent in.
     * @param guild      The {@link Guild} the command was sent in.
     * @param author     The {@link User} that sent the command
     * @param member     The {@link Member} that sent the command.
     * @param selfMember The self {@link Member} of the bot
     * @param selfUser The {@link SelfUser} of the bot
     * @param mentionedMembers A {@link List} containing the mentioned {@link Member}s
     */
    void handle(List<String> args,
                GuildMessageReceivedEvent event,
                JDA jda,
                TextChannel channel,
                Guild guild,
                User author,
                Member member,
                Member selfMember,
                SelfUser selfUser,
                List<Member> mentionedMembers);

    /**
     * Provides help for a command.
     *
     * @return The command help
     */
    String getHelp();

    /**
     * Provides the name of a command.
     *
     * @return The command name
     */
    String getInvoke();

    /**
     * Provides usage information for a command.
     *
     * @return The usage information
     */
   default String getUsage()  {
	   return String.format("%s%s", Constants.PREFIX, this.getInvoke());
   }

    /**
     * Provides the {@link net.dv8tion.jda.api.Permission}s the bot requires
     *
     * @return The Permissions
     */
    default Permission[] getBotPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    /**
     * Provides the {@link net.dv8tion.jda.api.Permission}s the {@link net.dv8tion.jda.api.entities.Member} requires
     *
     * @return The Permissions
     */
    default Permission[] getUserPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    /**
     * Provides the aliases of a command
     *
     * @return The aliases
     */
    default String[] getAliases() {return new String[0];}

    /**
     * Provides the category of a command
     *
     * @return The category
     */
    String getCategory();

    default boolean ownerCommand() {
        return false;
    }
}
