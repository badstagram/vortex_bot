package com.gitlab.badstagram.vortex.objects;

public class ResponseCode {
    public static final int OK = 200;
    public static final int FORBIDDEN = 403;
    public static final int UNAUTHORISED = 401;
}
