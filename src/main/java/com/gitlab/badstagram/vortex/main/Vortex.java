package com.gitlab.badstagram.vortex.main;

import com.gitlab.badstagram.vortex.listeners.*;
import com.gitlab.badstagram.vortex.utils.Config;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import com.gitlab.badstagram.vortex.utils.Utils;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import net.explodingbush.ksoftapi.KSoftAPI;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.net.URL;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Vortex {
    private static JDA api = null;
    private static KSoftAPI kSoftAPI = null;
    private static OkHttpClient client;
    private static final EventWaiter waiter = new EventWaiter();
    private static final Logger logger = LoggerFactory.getLogger(Vortex.class);
    private static final List<Long> afkUsers = new ArrayList<>();

    public static void main(String[] args) {
        Config.init();
        try {
            api = login(Config.getObject("tokens").getJSONObject("discord").getString("stable"));


        } catch (Exception e) {
            logger.error("There was an error and Vortex was unable to start up. Check sentry for more info.");
            ErrorHandler.handleExceptionNoMessage(e);
            System.exit(-1);
        }

        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleAtFixedRate(sendUptimeHeartbeat, 0, 1, TimeUnit.MINUTES);

    }

    public static JDA getApi() {
        return api;
    }

    public static KSoftAPI getKsoftApi() {
        if (kSoftAPI == null) {
            kSoftAPI = new KSoftAPI(Config.getObject("tokens").getString("ksoft"));
        }
        return kSoftAPI;
    }

    public static OkHttpClient getHttpClient() {
        if (client == null) {
            client = new OkHttpClient();
        }
        return client;
    }

    protected static JDA login(String token) throws LoginException, InterruptedException {
        return JDABuilder.create(token, EnumSet.of(GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_EMOJIS, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_PRESENCES))
                .disableCache(CacheFlag.VOICE_STATE)
                .disableCache(CacheFlag.ACTIVITY)
                .setActivity(Activity.playing("Loading..."))
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .addEventListeners(
                        waiter,
                        new OnReady(),
                        new OnGuildJoin(),
                        new OnGuildLeave(),
                        new OnGuildMessageReceived(),
                        new OnDisconnect()).build().awaitReady();
    }

    static final Runnable sendUptimeHeartbeat = () -> {
        Request request;

        try {
            OkHttpClient httpClient = getHttpClient();

            request = new Request.Builder()
                    .url(new URL(Config.getString("uptimeHeartbeatUrl"))).build();

            httpClient.newCall(request).execute().close();
        } catch (Exception e) {
            ErrorHandler.handleExceptionNoMessage(e);
        }
    };

    public static String getReleaseType() {
        if (getApi().getSelfUser().getIdLong() == 702129848910610452L) {
            return "Production";
        } else if (getApi().getSelfUser().getIdLong() == 703651420817195048L) {
            return "Alpha";
        }
        return " ";
    }

    public static EventWaiter getWaiter() {
        return waiter;
    }

    public static List<Long> getAfkUsers() {
        return afkUsers;
    }
}


