package com.gitlab.badstagram.vortex.utils;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class Config {
    private Config(){}

    private static final File cfgFile = new File("./config.json");
    private static JSONObject config;

    public static void init() {
        try (InputStream in = new FileInputStream(cfgFile)) {
            String content = IOUtils.toString(in, StandardCharsets.UTF_8);
            config = new JSONObject(content);
        } catch (Exception e) {
            ErrorHandler.handleExceptionNoMessage(e);
        }
    }

    public static JSONObject getConfig() {return config;}

    public static JSONObject getObject(String key) {return config.getJSONObject(key);}

    public static String getString(String key) {return config.getString(key);}
}
