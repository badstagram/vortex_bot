package com.gitlab.badstagram.vortex.utils;

public class Enums {

    public enum PunishmentType {
        BAN,
        KICK,
        MUTE
    }

    public enum ConfigOption {
        MUTED_ROLE,
        MOD_LOG,
        PUNISH_LOG
    }

    public enum Module {
        ADMIN,
        FUN,
        HELP,
        INFO,
        MODERATION
    }
}

