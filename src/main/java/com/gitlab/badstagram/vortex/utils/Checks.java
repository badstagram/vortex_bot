package com.gitlab.badstagram.vortex.utils;

import com.gitlab.badstagram.vortex.Constants;
import net.dv8tion.jda.api.entities.User;

public class Checks {
    private Checks(){}



    public static boolean owner(User author) {
        return author.getIdLong() == Constants.OWNER_ID || author.getIdLong() == 703651420817195048L;
    }
}

