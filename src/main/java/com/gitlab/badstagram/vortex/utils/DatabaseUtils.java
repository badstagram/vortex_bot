package com.gitlab.badstagram.vortex.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseUtils {
    private DatabaseUtils(){}
    private static Connection connection = null;

    public static Connection getConnection(String databaseName) throws SQLException {
        if (connection == null) {
            connection = DriverManager.getConnection("jdbc:sqlite:C:/vortex/database/" + databaseName);
        }
        return connection;
    }
}

