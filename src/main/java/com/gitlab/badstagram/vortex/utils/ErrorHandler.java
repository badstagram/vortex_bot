package com.gitlab.badstagram.vortex.utils;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import io.sentry.Sentry;
import io.sentry.event.Breadcrumb;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import java.awt.*;

public class ErrorHandler {
    private ErrorHandler() {
    }

    /**
     * Logs a {@link java.lang.Throwable} to sentry and informs the user of the error.
     *
     * @param thr               The Exception
     * @param event             The event
     * @param includeStackTrace If the stacktrace should be logged.
     * @author badstagram
     */
    public static void handleException(Throwable thr, GuildMessageReceivedEvent event, boolean includeStackTrace) {
        Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
        logger.error(thr.getMessage());
        // Inform user there was an exception.
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Command raised an exception.");
        eb.setDescription(MarkdownUtil.monospace(thr.getMessage()));
        eb.setColor(new Color(255, 0, 0));
        String[] typeSplit = thr.getClass().getName().split("\\.");
        eb.setFooter(String.format("This exception has been reported to my developer. | For support, join our support guild: %s | %s", Constants.SUPPORT_SERVER, typeSplit[typeSplit.length - 1]));
        event.getChannel().sendMessage(eb.build()).queue();

        if (includeStackTrace) {
            logger.error("Stack Trace");
            for (StackTraceElement ste : thr.getStackTrace()) {
                logger.error(ste.toString());
            }
        }

        //Log exception to sentry
        Sentry.init(Config.getConfig().getJSONObject("tokens").getString("sentry-dsn"));

        Sentry.getContext().recordBreadcrumb(
                new BreadcrumbBuilder()
                        .withData("Message", event.getMessage().getContentRaw())
                        .withData("Guild Id", event.getGuild().getId())
                        .withData("Guild Name", event.getGuild().getName())
                        .setLevel(Breadcrumb.Level.ERROR).build()
        );
        Sentry.getContext().setUser(
                new UserBuilder()
                        .setUsername(event.getAuthor().getAsTag()).build()
        );


        Sentry.getContext().addTag("Environment", Vortex.getReleaseType());
        Sentry.capture(thr);
    }


    /**
     * Logs a {@link java.lang.Throwable} to sentry but doesn't inform the user.
     *
     * @param thr The {@link java.lang.Throwable} to log
     * @author badstagram
     */
    public static void handleExceptionNoMessage(Throwable thr) {
        //Log exception to sentry
        Sentry.init(Config.getObject("tokens").getString("sentryDsn"));
        Sentry.capture(thr);

        Logger log = LoggerFactory.getLogger(ErrorHandler.class);
        log.error(thr.getMessage(), thr);
    }

    /**
     * Informs the user they do not have owner level permissions.
     *
     * @param event The event
     * @author badstagram
     */
    public static void noOwner(GuildMessageReceivedEvent event) {
        User user = event.getAuthor();
        Guild guild = event.getGuild();
        event.getChannel().sendMessage("You do not have permission for this command!").queue();
        Utils.log(ErrorHandler.class, String.format("User was denied access to admin command! %nUser: %s (%s), Guild: %s (%s), Command: eval", user.getAsTag(), user.getId(), guild.getName(), guild.getId()), Level.WARN);
    }

    /**
     * Informs the user they did not use the correct syntax.
     *
     * @param event The event
     * @author badstagram
     */
    public static void invalidSyntax(GuildMessageReceivedEvent event, ICommand command) {

        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("There was an error running that command.");
        eb.setDescription("`Invalid syntax`");
        eb.setFooter(String.format("Usage: %s | For support, join our support guild: %s", command.getUsage(), Constants.SUPPORT_SERVER));

        event.getChannel().sendMessage(eb.build()).queue();
    }
}

