package com.gitlab.badstagram.vortex;

import com.gitlab.badstagram.vortex.commands.admin.*;
import com.gitlab.badstagram.vortex.commands.fun.*;
import com.gitlab.badstagram.vortex.commands.help.Help;
import com.gitlab.badstagram.vortex.commands.help.Invite;
import com.gitlab.badstagram.vortex.commands.info.*;
import com.gitlab.badstagram.vortex.commands.moderation.*;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CommandManager {
    private static final Map<String, ICommand> commands = new HashMap<>();
    

    public CommandManager() {
        addCommand(new Ping());
        addCommand(new ServerInfo());
        addCommand(new Invite());
        addCommand(new Meme());
        addCommand(new Eval());
        addCommand(new BotInfo());
        addCommand(new UserInfo());
        addCommand(new Config());
        addCommand(new Ban());
        addCommand(new TempBan());
        addCommand(new SuperBan());
        addCommand(new CheckBan());
        addCommand(new Embed());
        addCommand(new Joke());
        addCommand(new DadJoke());
        addCommand(new Status());
        addCommand(new Update());
        addCommand(new Restart());
        addCommand(new Hentai());
        addCommand(new News());
        addCommand(new AddEmote());
        addCommand(new CharInfo());
        addCommand(new Help(getCommands()));
        addCommand(new HttpCat());
        addCommand(new Clear());
        addCommand(new ClearUser());
        addCommand(new ClearUntil());
        addCommand(new RoleInfo());
        addCommand(new SoftwareGore());
        addCommand(new ProgrammerHumor());
        addCommand(new RedditCommand());
        addCommand(new AFK());
        addCommand(new AddRole());
        addCommand(new RemoveRole());
        addCommand(new Disable());
        addCommand(new Enable());
        addCommand(new ForceBan());
        addCommand(new GuildInfo());
    }

    public static Map<String, ICommand> getCommands() {
        return commands;
    }

    private static final List<ICommand> disabledCommands = new ArrayList<>();
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private void addCommand(ICommand command) {
        if (commands.containsKey(command.getInvoke())) {
            throw new IllegalArgumentException(String.format("Command with name %s already exists", command.getInvoke()));
        }
        this.logger.info("Command \"{}\" registered.", command.getInvoke());
        commands.put(command.getInvoke(), command);
    }

    public void handleCommand(GuildMessageReceivedEvent event) {
        String[] split;
        String invoke;
        String content = event.getMessage().getContentRaw().toLowerCase(Locale.UK);
        if (content.startsWith(Constants.PREFIX)) {
            split = event.getMessage().getContentRaw().replaceFirst("(?i)" + Pattern.quote(Constants.PREFIX), "")
                    .split("\\s+");
            invoke = split[0].toLowerCase(Locale.UK);

            checkAndHandle(split, invoke, event);
        }
    }

    /**
     * @param member      The {@link Member} to check
     * @param permissions The {@link Permission}s to check
     * @return If the {@link Member} has all the {@link Permission}s
     */
    private boolean hasPermissions(Member member, Collection<Permission> permissions) {
        return member.hasPermission(permissions) || member.hasPermission(Permission.ADMINISTRATOR);
    }

    private void checkAndHandle(String[] split, String invoke, GuildMessageReceivedEvent event) {

        if (commands.containsKey(invoke)) {
            final List<String> args = Arrays.asList(split).subList(1, split.length);
            ICommand command = commands.get(invoke);
            if (disabledCommands.contains(command)) {
                event.getChannel().sendMessage("That command is currently disabled.").queue();
                return;
            }

            if (command.ownerCommand() && event.getMember().getIdLong() != Constants.OWNER_ID) {
                ErrorHandler.noOwner(event);
                return;
            }

            List<Permission> botPermissions = Arrays.asList(command.getBotPermissions());
            List<Permission> userPermissions = Arrays.asList(command.getUserPermissions());

            if (!hasPermissions(event.getGuild().getSelfMember(), botPermissions)) {
                List<String> perms = userPermissions.stream()
                        .map(Permission::getName)
                        .collect(Collectors.toList());

                event.getChannel().sendMessage(String.format("I do not have permissions for that command. Required: %s", String.join(", ", perms))).queue();
                return;
            }

            if (!hasPermissions(event.getMember(), userPermissions)) {


                List<String> perms = userPermissions.stream()
                        .map(Permission::getName)
                        .collect(Collectors.toList());

                event.getChannel().sendMessage(String.format("You do not have permissions for that command. Required: %s", String.join(", ", perms))).queue();
                return;
            }

            if (event.getMessage().getContentRaw().toLowerCase().startsWith(Constants.PREFIX.toLowerCase(Locale.UK))) {
                command.handle(
                        args,
                        event,
                        event.getJDA(),
                        event.getChannel(),
                        event.getGuild(),
                        event.getAuthor(),
                        event.getMember(),
                        event.getGuild().getSelfMember(),
                        event.getJDA().getSelfUser(),
                        event.getMessage().getMentionedMembers());
            }
        }
    }


    /**
     * Gets a {@link ICommand} by the name of the command.
     *
     * @param name The name of the command
     * @return The resulting {@link ICommand}.
     */
    @Nullable
    public static ICommand getCommandByName(String name) {
        return commands.getOrDefault(name, null);
    }

    public static List<ICommand> getAsList() {
        List<ICommand> cmds = new ArrayList<>();
        for (Map.Entry<String, ICommand> entry : commands.entrySet()) {
            ICommand command = entry.getValue();
            cmds.add(command);
        }

        return cmds;
    }

    public static String getUserPermissions(ICommand command) {
        List<Permission> perms = Arrays.asList(command.getUserPermissions());
        return perms.isEmpty() ?
                "None" :
                perms.stream()
                        .map(Permission::getName)
                        .collect(Collectors.joining(", "));
    }

    public static String getBotPermissions(ICommand command) {
        List<Permission> perms = Arrays.asList(command.getBotPermissions());
        return perms.isEmpty() ?
                "None" :
                perms.stream()
                        .map(Permission::getName)
                        .collect(Collectors.joining(", "));
    }

    public static void disableCommand(ICommand command) {
        if (disabledCommands.contains(command)) {
            throw new IllegalArgumentException("Command is already disabled.");
        }
        disabledCommands.add(command);
    }

    public static void enableCommand(ICommand command) {
        if (!disabledCommands.contains(command)) {
            throw new IllegalArgumentException("Command is not disabled.");
        }
        disabledCommands.remove(command);
    }
}
