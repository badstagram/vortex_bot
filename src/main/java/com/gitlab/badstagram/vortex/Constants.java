package com.gitlab.badstagram.vortex;

import com.gitlab.badstagram.vortex.main.Vortex;

public class Constants {
    private Constants(){}
    // Config
    public static final String PREFIX = Vortex.getReleaseType().equalsIgnoreCase("production") ? "-" : "/";
    public static final long OWNER_ID = 424239181296959507L;

    //Emotes
    public static final String BOT_EMOTE = "<:BOT:629030492838166538>";
    public static final String SUPPORT_SERVER = "https://discord.gg/ZM9ftwE";
}

