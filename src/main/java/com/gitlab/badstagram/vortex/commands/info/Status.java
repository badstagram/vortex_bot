package com.gitlab.badstagram.vortex.commands.info;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import com.gitlab.badstagram.vortex.utils.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class Status implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        channel.sendTyping().queue();
        try {
            final String STATUS = "status";
            EmbedBuilder eb = new EmbedBuilder().setAuthor("Status", "https://www.youtube.com/watch?v=dQw4w9WgXcQ", null);
            JSONObject discordStatus = getStatus("https://srhpyqt94yxb.statuspage.io/api/v2/status.json").getJSONObject(STATUS);
            JSONObject cloudflareStatus = getStatus("https://yh6f0r4529hb.statuspage.io/api/v2/status.json").getJSONObject(STATUS);
            JSONObject galaxygateStatus = getStatus("https://statuspal.io/api/v1/status_pages/galaxygate/status").getJSONObject("status_page");
            eb.appendDescription(MarkdownUtil.maskedLink("Discord: ", "https://status.discordapp.com"))
                    .appendDescription(discordStatus.getString("description"))
                    .appendDescription("\n")
                    .appendDescription(MarkdownUtil.maskedLink("CloudFlare: ", "https://cloudflarestatus.com"))
                    .appendDescription(cloudflareStatus.getString("description"))
                    .appendDescription("\n")
                    .appendDescription(MarkdownUtil.maskedLink("GalaxyGate: ", "https://status.galaxygate.net"))
                    .appendDescription(galaxygateStatus.get("current_incident_type").equals(JSONObject.NULL) ? "All Systems Operational" : galaxygateStatus.getString("current_incident_type"))
                    .appendDescription("\n");

            channel.sendMessage(eb.build()).queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Gets status information about the various services used by Vortex";
    }

    @Override
    public String getInvoke() {
        return "status";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "info";
    }

    private JSONObject getStatus(final String url) throws IOException {
        OkHttpClient client = Vortex.getHttpClient();
        Request request = new Request.Builder()
                .url(new URL(url))
                .addHeader("Accept", "application/json").build();

        Response response = client.newCall(request).execute();

        return new JSONObject(response.body().string());


    }
}

