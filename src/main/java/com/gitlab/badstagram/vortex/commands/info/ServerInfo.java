package com.gitlab.badstagram.vortex.commands.info;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.Region;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ServerInfo implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        Region guildVoiceRegion = guild.getRegion();
        String regionFlag = guildVoiceRegion.getEmoji();
        String regionName = guildVoiceRegion.getName();
        List<Member> memberList = guild.getMembers();

        long bots = memberList.stream().map(Member::getUser).filter(User::isBot).count();
        long humans = memberList.size() - bots;

        int bans = guild.retrieveBanList().complete().size();
        OffsetDateTime guildCreatedTime = guild.getTimeCreated();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");


        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(guild.getName());
        eb.setThumbnail(guild.getIconUrl());


        eb.addField("id", guild.getId(), true);
        eb.addField("Owner", guild.retrieveOwner().complete().getAsMention(), true);
        eb.addField("Member Count", String.format("%d Members%n%d Bots%n%d Humans", memberList.size(), bots, humans), false);
        eb.addField("Voice Region", String.format("%s %s", regionFlag, regionName), false);
        eb.addField("Ban Count", String.valueOf(bans), false);
        eb.addField("Date Created", guildCreatedTime.format(formatter), false);


        event.getChannel().sendMessage(eb.build()).queue();

    }

    @Override
    public String getHelp() {
        return "Gets info about a guild";
    }

    @Override
    public String getInvoke() {
        return "serverinfo";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "info";
    }
}

