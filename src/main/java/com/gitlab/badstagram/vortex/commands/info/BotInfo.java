package com.gitlab.badstagram.vortex.commands.info;

import com.gitlab.badstagram.vortex.CommandManager;
import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDAInfo;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.util.List;

public class BotInfo implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {


        EmbedBuilder eb = new EmbedBuilder();
        jda.retrieveUserById(Constants.OWNER_ID).submit().thenAccept(developer -> {
            int commandCount = CommandManager.getCommands().size();


            Logger logger = LoggerFactory.getLogger(this.getClass());
            String jdaVersion = JDAInfo.VERSION;

            eb.setAuthor("Bot info", "https://www.youtube.com/watch?v=dQw4w9WgXcQ", event.getAuthor().getAvatarUrl());
            eb.addField("Developer", developer.getAsTag(), true);
            eb.addField("Command Count", String.valueOf(commandCount), true);
            eb.addField("Library", "JDA", true);
            eb.addField("Version", jdaVersion, true);
            eb.addField("Uptime", Utils.getUptime(), false);
            eb.addField("Guilds", String.valueOf(jda.getGuilds().size()), true);
            eb.addField("Members", String.valueOf(jda.getUserCache().size()), true);
        });



        event.getChannel().sendMessage(eb.build()).queue();

    }

    @Override
    public String getHelp() {
        return "Gets info about the bot";
    }

    @Override
    public String getInvoke() {
        return "botinfo";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[]{"test"};
    }

    @Override
    public String getCategory() {
        return "info";
    }
}

