package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

import java.net.URL;
import java.util.List;

public class DadJoke implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        try {
            Request request;
            Response response;
            OkHttpClient client = Vortex.getHttpClient();
            request = new Request.Builder()
                    .url(new URL("https://icanhazdadjoke.com/"))
                    .addHeader("Accept", "application/json")
                    .addHeader("User-Agent", "Vortex Bot (http://vortex-bot.xyz:32768/badstagram/Vortex)").build();
            response = client.newCall(request).execute();
            JSONObject json = new JSONObject(response.body().string());

            if (response.code() != 200 || json.getInt("status") != 200) {
                event.getChannel().sendMessage("Received unexpected response.").queue();
                return;
            }

            event.getChannel().sendMessage(MarkdownUtil.spoiler(json.getString("joke"))).queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Sends a dad joke";
    }

    @Override
    public String getInvoke() {
        return "dadjoke";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    @Override
    public Permission[] getUserPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}

