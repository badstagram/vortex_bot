package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

import java.net.URL;
import java.util.List;

public class Joke implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        try {
        OkHttpClient client = Vortex.getHttpClient();
        Request request;
        Response response;
        URL url;
        if (event.getChannel().isNSFW()) {
            url = new URL("https://sv443.net/jokeapi/v2/joke/Any?type=twopart");
        } else {
            url = new URL("https://sv443.net/jokeapi/v2/joke/Any?blacklistFlags=nsfw,racist&format=json&type=twopart");
        }

            request = new Request.Builder()
                    .url(url)
                    .addHeader("Accept", "application/json").build();

            response = client.newCall(request).execute();

            JSONObject json = new JSONObject(response.body().string());
            JSONObject flags = json.getJSONObject("flags");
            boolean nsfw = flags.getBoolean("nsfw");
            boolean religious = flags.getBoolean("religious");
            boolean racist = flags.getBoolean("racist");
            boolean sexist = flags.getBoolean("sexist");
            EmbedBuilder eb = new EmbedBuilder();

            eb.setTitle(json.getString("setup"));
            eb.setDescription(MarkdownUtil.spoiler(json.getString("delivery")));
            eb.setFooter(String.format("Flags: %s%s%s%s %nJoke from JokeAPI | Joke ID: %d | Joke Category: %s",
                    nsfw ? "nsfw" : "",
                    religious ? "religious" : "",
                    racist ? "racist" : "",
                    sexist ? "sexist" : "",
                    json.getInt("id"),
                    json.getString("category")).trim());


            event.getChannel().sendMessage(eb.build()).queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Sends a joke. Use in NSFW channels for NSFW and racist jokes";
    }

    @Override
    public String getInvoke() {
        return "joke";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}

