package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.Enums;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import com.gitlab.badstagram.vortex.utils.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.util.List;

public class Config implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event, this);
        }


        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Configuration updated");
        eb.setColor(new Color(255, 0, 251));

        try {
            switch (args.get(0)) {
                case "mod-log":
                    Utils.setConfigSetting(event.getGuild().getId(), args.get(1), Enums.ConfigOption.MOD_LOG);
                    event.getChannel().sendMessage(eb.build()).queue();
                    break;
                case "punish-log":
                    Utils.setConfigSetting(event.getGuild().getId(), args.get(1), Enums.ConfigOption.PUNISH_LOG);
                    event.getChannel().sendMessage(eb.build()).queue();
                    break;
                case "muted-role":
                    Utils.setConfigSetting(event.getGuild().getId(), args.get(1), Enums.ConfigOption.MUTED_ROLE);
                    event.getChannel().sendMessage(eb.build()).queue();
                    break;
                default:
                    eb.clear();
                    eb.setTitle("There was an error running that command!");
                    eb.setDescription("Invalid Syntax");
                    eb.setFooter(String.format("Replace spaces with dashes [-] | For support join our support guild: %s", Constants.SUPPORT_SERVER));

                    event.getChannel().sendMessage(eb.build()).queue();
                    break;
            }

        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }

    }

    @Override
    public String getHelp() {
        return "Changes configuration options for the guild";
    }

    @Override
    public String getInvoke() {
        return "config";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <setting> <value>", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{Permission.MANAGE_SERVER};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

