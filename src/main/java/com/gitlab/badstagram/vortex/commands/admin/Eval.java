package com.gitlab.badstagram.vortex.commands.admin;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import groovy.lang.GroovyShell;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;

import java.awt.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Eval implements ICommand {
    private final GroovyShell engine = new GroovyShell();
    private static final String IMPORTS = "import java.io.*\n" +
            "import java.lang.*\n" +
            "import java.util.*\n" +
            "import java.util.concurrent.*\n" +
            "import net.dv8tion.jda.api.*\n" +
            "import net.dv8tion.jda.api.entities.*\n" +
            "import net.dv8tion.jda.api.entities.impl.*\n" +
            "import net.dv8tion.jda.api.managers.*\n" +
            "import net.dv8tion.jda.api.managers.impl.*\n" +
            "import net.dv8tion.jda.api.utils.*\n";


    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {


        String script = event.getMessage().getContentRaw().split("\\s+", 2)[1];

        if (Vortex.getReleaseType().equalsIgnoreCase("alpha")) {
            runEval(script, args, event);

        } else if (Vortex.getReleaseType().equalsIgnoreCase("production")){
            EventWaiter waiter = Vortex.getWaiter();
            EmbedBuilder eb = new EmbedBuilder();
            eb.setColor(Color.YELLOW);
            eb.setTitle("Warning!");
            eb.setDescription("This is a dangerous command running on the production build!");
            eb.setFooter("Please confirm you want to run this command. Type \"yes\" or \"no\"");
            Message message = event.getChannel().sendMessage(eb.build()).complete();


            waiter.waitForEvent(GuildMessageReceivedEvent.class, e -> e.getChannel().equals(event.getChannel()) && e.getAuthor().equals(event.getAuthor()),
                    e -> {
                        Message m = e.getMessage();

                        if (m.getContentRaw().equalsIgnoreCase("yes")) {
                            message.delete().queue();
                            m.delete().queue();
                            runEval(script, args, event);
                        } else if (m.getContentRaw().equalsIgnoreCase("no")) {
                            message.delete().queue();
                            m.delete().queue();
                            e.getChannel().sendMessage(":x: Canceled.").queue();
                        }
                    }, 30, TimeUnit.SECONDS, () -> {
                        message.delete().queue();
                        event.getChannel().sendMessage(":x: Timed out.").queue();
                    });
        }

    }


    private void runEval(String code, List<String> args, GuildMessageReceivedEvent event) {
        try {
            engine.setProperty("args", args);
            engine.setProperty("event", event);
            engine.setProperty("message", event.getMessage());
            engine.setProperty("channel", event.getChannel());
            engine.setProperty("jda", event.getJDA());
            engine.setProperty("guild", event.getGuild());
            engine.setProperty("member", event.getMember());
            engine.setProperty("runtime", Runtime.getRuntime());
            engine.setProperty("vortex", Vortex.class);

            Object out = engine.evaluate(IMPORTS + code);
            String[] typeArray = out.getClass().getName().split("\\.");

            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("Eval Success");
            eb.setColor(new Color(0,255,0));
            eb.addField("Input", MarkdownUtil.codeblock("java", code), false);
            eb.addField("Output", MarkdownUtil.codeblock("java", out.toString()), false);
            eb.addField("Type", MarkdownUtil.codeblock("java", typeArray[typeArray.length - 1]), false);

            event.getChannel().sendMessage(eb.build()).queue();
        } catch (Exception e) {
            String[] typeArray = e.getClass().getName().split("\\.");

            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("Eval Error");
            eb.setColor(new Color(255,0,0));
            eb.addField("Input", MarkdownUtil.codeblock("java", code), false);
            eb.addField("Output", MarkdownUtil.codeblock("java", e.getMessage()), false);
            eb.addField("Type", MarkdownUtil.codeblock("java", typeArray[typeArray.length - 1]), false);
            event.getChannel().sendMessage(eb.build()).queue();
        }
    }


    @Override
    public String getHelp() {
        return "owner command: takes groovy code and runs it";
    }

    @Override
    public String getInvoke() {
        return "eval";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "admin";
    }

    @Override
    public boolean ownerCommand() {
        return true;
    }
}
