package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class Embed implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        List<String> arg = Arrays.asList(String.join(" ", args).split(" \\| "));
        EmbedBuilder eb = new EmbedBuilder();
        event.getMessage().delete().queue();
        if (arg.size() < 3) {
            eb.setTitle("There was an error running that command.");
            eb.setDescription("`Invalid syntax.`");
            eb.setFooter(String.format("Usage: %s | For support, join our support guild: %s", getUsage(), Constants.SUPPORT_SERVER));

            event.getChannel().sendMessage(eb.build()).queue();

            eb.clear();
            return;
        }
        event.getMessage().delete().queue(null, ignored -> {});
        String title = arg.get(0);
        Color colour = new Color(Integer.parseInt(arg.get(1).replace("0x", ""), 16));
        String message = arg.get(2).replace("/n", "\n");

        eb.setTitle(title);
        eb.setColor(colour);
        eb.setDescription(message);
        eb.setFooter("Requested By: "+event.getAuthor().getAsTag());

        event.getChannel().sendMessage(eb.build()).queue();
    }

    @Override
    public String getHelp() {
        return "Sends an embedded message to the channel.";
    }

    @Override
    public String getInvoke() {
        return "embed";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <title> | <hex colour> | <message>", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}

