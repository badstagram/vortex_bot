package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Reddit;
import net.explodingbush.ksoftapi.enums.ImageType;

import java.util.List;

public class RedditCommand implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }

        try {
            KSoftAPI kSoftAPI = Vortex.getKsoftApi();
            Reddit post = kSoftAPI.getRedditImage(ImageType.RANDOM_REDDIT)
                    .setSubreddit(args.get(0))
                    .execute();

            if (!channel.isNSFW() && post.isNsfw()) {
                channel.sendMessage("Channel must be marked as NSFW in order too post NSFW reddit posts").queue();
                return;
            }


            if (!post.subredditExists()) {
                channel.sendMessage(String.format("Subreddit `r/%s` does not exist!", args.get(0))).queue();
                return;
            }
            EmbedBuilder eb = new EmbedBuilder();
            eb.setAuthor(post.getAuthor(), post.getSourceUrl(), null);
            eb.setImage(post.getImageUrl());
            eb.setFooter(String.format("Posted in %s by %s | Upvotes: %d", post.getSubreddit(), post.getAuthor(), post.getUpvotesInt()));

            channel.sendMessage(eb.build()).queue();

        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Gets a post from a specified subreddit";
    }

    @Override
    public String getInvoke() {
        return "reddit";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <subreddit>", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[0];
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}

