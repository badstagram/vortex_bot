package com.gitlab.badstagram.vortex.commands.info;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;

import java.util.List;

public class Ping implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        long gatewayPing = jda.getGatewayPing();
        event.getChannel().sendMessage("Pinging...").submit().thenCompose((message -> {
            jda.getRestPing().queue(restPing -> {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle("Ping!");
                eb.addField("Gateway", MarkdownUtil.monospace(gatewayPing + "ms"), false);
                eb.addField("Rest", MarkdownUtil.monospace(restPing + "ms"), false);
                message.editMessage(" ").embed(eb.build()).queue();
            });
            return null;
        }));

    }

    @Override
    public String getHelp() {
        return "Gets the ping to discord.";
    }

    @Override
    public String getInvoke() {
        return "ping";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_WRITE};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[] {"howshitisdiscord"};
    }

    @Override
    public String getCategory() {
        return "info";
    }
}

