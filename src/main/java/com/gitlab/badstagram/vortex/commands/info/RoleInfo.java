package com.gitlab.badstagram.vortex.commands.info;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import com.gitlab.badstagram.vortex.utils.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class RoleInfo implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }
        EmbedBuilder eb = new EmbedBuilder();
        String rName = String.join(" ", args);
        List<Role> roles = guild.getRolesByName(rName, true);
        Role role;
        if (rName.split("\\s+")[0].equalsIgnoreCase("everyone")) {
            role = guild.getPublicRole();
        } else {
            if (roles.isEmpty()) {
                channel.sendMessage("No found with that name.").queue(m -> m.delete().queueAfter(1, TimeUnit.MINUTES));
                return;
            }
            role = guild.getRolesByName(rName, true).get(0);
        }

        Color roleColor = role.getColor() == null ? new Color(0x95A5A6) : role.getColor();
        int r, g, b;
        EnumSet<Permission> permissions = role.getPermissions();
        StringBuilder sb = new StringBuilder();
        if (!permissions.isEmpty()) {
            for (Permission p : permissions) {
                sb.append(p.getName()).append(", ");
            }
        }

        r = roleColor.getRed();
        g = roleColor.getGreen();
        b = roleColor.getBlue();
        List<String> members;
        if (role.equals(guild.getPublicRole())) {
            members = guild.getMembers().stream()
                    .map(Member::getUser)
                    .map(User::getAsMention)
                    .collect(Collectors.toList());
        } else {
            members = guild.getMembersWithRoles(role).stream()
                    .map(Member::getUser)
                    .filter(u -> !u.isBot())
                    .map(User::getAsMention)
                    .collect(Collectors.toList());
        }


        eb.setTitle("Role info");
        eb.setColor(roleColor);

        eb.addField("Name", role.getName(), true);
        eb.addField("ID", role.getId(), true);
        eb.addField("Mention", role.getAsMention(), true);
        eb.addField("Colour", String.format("0x%X%X%X", r, g, b), true);
        eb.addField("Position", String.format("%d / %d", role.getPosition() + 1, guild.getRoleCache().size()), true);
        eb.addField("Hoisted", role.isHoisted() ? "Yes" : "No", true);
        eb.addField("Mentionable", role.isMentionable() ? "Yes" : "No", true);
        eb.addField("Permissions", !permissions.isEmpty() ? Utils.replaceLast(sb.toString(), ", ", "") : "None", true);
        eb.addField("Members (count)", String.join(", ", members) + " (" + members.size() + ")", false);

        channel.sendMessage(eb.build()).queue();
    }

    @Override
    public String getHelp() {
        return "Gets information about a role.";
    }

    @Override
    public String getInvoke() {
        return "roleinfo";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[0];
    }

    @Override
    public String[] getAliases() {
        return new String[]{"role-info"};
    }

    @Override
    public String getCategory() {
        return "info";
    }
}

