package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class Ban implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        if (mentionedMembers.isEmpty() || args.size() < 2) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }


        String reason = String.join(" ", args.subList(1, args.size()));
        Member target = mentionedMembers.get(0);

        if (!member.canInteract(target) || !selfMember.canInteract(target)) {
            channel.sendMessageFormat(":x: **%s** could not be banned.", target.getUser().getAsTag()).queue();
            return;
        }

        target.getUser().openPrivateChannel().submit()
                .thenAcceptAsync(ch -> ch.sendMessageFormat("You have been banned from **%s** by **%s** for **%s**", guild.getName(), member.getUser().getAsTag(), reason).queue())
                .whenCompleteAsync((ignored_, ignored__) -> guild.ban(target, 7).reason(reason).submit()
                        .thenAcceptAsync(v -> channel.sendMessageFormat(":white_check_mark: **%s** (`%s`) has been banned: `%s`", target.getUser().getAsTag(), target.getId(), reason).queue())
                        .exceptionally(thr -> {
                            channel.sendMessageFormat(":x: **%s** could not be banned: `%s`", target.getUser().getAsTag(), thr.getMessage()).queue();
                            return null;
                        }));
    }

    @Override
    public String getHelp() {
        return "Bans a member from the guild.";
    }

    @Override
    public String getInvoke() {
        return "ban";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <@user> <reason>", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.BAN_MEMBERS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{Permission.BAN_MEMBERS};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

