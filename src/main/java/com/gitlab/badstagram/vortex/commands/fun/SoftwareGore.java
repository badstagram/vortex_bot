package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Reddit;
import net.explodingbush.ksoftapi.enums.ImageType;

import java.util.List;

public class SoftwareGore implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        try {
            KSoftAPI kSoftAPI = Vortex.getKsoftApi();
            Reddit post = kSoftAPI.getRedditImage(ImageType.RANDOM_REDDIT)
                    .setSubreddit("softwaregore")
                    .execute();

            EmbedBuilder eb = new EmbedBuilder();
            eb.setAuthor(post.getTitle(), post.getSourceUrl(), null);
            eb.setImage(post.getImageUrl());
            eb.setFooter(String.format("Posted in r/softwaregore by %s | Upvotes: %d", post.getAuthor(), post.getUpvotesInt()));

            channel.sendMessage(eb.build()).queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "gets a random image from r/softwaregore.";
    }

    @Override
    public String getInvoke() {
        return "softwaregore";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    @Override
    public String[] getAliases() {
        return new String[] {"sg"};
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}

