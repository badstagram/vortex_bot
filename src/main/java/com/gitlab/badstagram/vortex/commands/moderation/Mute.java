package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class Mute implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        // Not Yet Implemented
    }

    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public String getInvoke() {
        return null;
    }

    @Override
    public String getUsage() {
        return null;
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[0];
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[0];
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

