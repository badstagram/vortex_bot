package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.Enums;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import com.gitlab.badstagram.vortex.utils.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.util.List;

public class TempBan implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        try {


            //          0       1       2
            // v!ban <@user> <Time> <reason>
            if (args.size() < 3 || mentionedMembers.isEmpty()) {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle("There was an error running that command.");
                eb.setDescription("`Invalid syntax`");
                eb.setFooter(String.format("Usage: %s | For support, join our support guild: %s", getUsage(), Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                return;
            }

            Member moderator = event.getMember();
            Member offender = mentionedMembers.get(0);
            String reason = String.join(" ", args.subList(2, args.size()));

            if (!moderator.canInteract(offender) || !event.getGuild().getSelfMember().canInteract(offender)) {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle("There was an error running that command.");
                eb.setDescription("`That member can not be banned.`");
                eb.setFooter(String.format("Usage: %s | For support, join our support guild: %s", getUsage(), Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                return;
            }
            final String time = args.subList(1, args.size()).get(0);


            EmbedBuilder eb = new EmbedBuilder();
            eb.setColor(new Color(255, 0, 0));
            eb.setTitle("Banned");
            eb.appendDescription(String.format("**Server**: %s%n", event.getGuild().getName()));
            eb.appendDescription(String.format("**Banned By**: %s%n", moderator.getUser().getAsTag()));
            eb.appendDescription(String.format("**Reason**: %s%n", reason));

            offender.getUser().openPrivateChannel().submit().thenCompose(privateChannel -> channel.sendMessage(eb.build()).submit().thenCompose(ignored -> {
                Utils.addPunishmentToDb(offender.getId(), moderator.getId(), reason, event.getGuild().getId(), Enums.PunishmentType.BAN, time, event);
                return null;
            })).whenComplete((ignored, ign) ->
                event.getGuild().ban(offender, 7).reason(reason).queue(igno -> {
                    eb.clear();
                    eb.setColor(new Color(255, 0, 0));
                    eb.setTitle("User Banned");
                    eb.appendDescription(String.format("**User**: %s (%s)%n", offender.getUser().getAsTag(), offender.getId()));
                    eb.appendDescription(String.format("**Banned By**: %s%n", moderator.getUser().getAsTag()));
                    eb.appendDescription(String.format("**Duration**: %s%n", time));
                    eb.appendDescription(String.format("**Reason**: %s%n", reason));

                    event.getChannel().sendMessage(eb.build()).queue();
                }));
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Bans a member for a given period of time";
    }

    @Override
    public String getInvoke() {
        return "tempban";
    }

    @Override
    public String getUsage() {
        return "%s%s <@Member> <Time> <Reason>";
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.BAN_MEMBERS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{Permission.BAN_MEMBERS};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

