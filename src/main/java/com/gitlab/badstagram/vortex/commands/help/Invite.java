package com.gitlab.badstagram.vortex.commands.help;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;

import java.util.List;

public class Invite implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle("Invite");
        eb.addField("Bot Invite", MarkdownUtil.maskedLink("Here", event.getJDA().getInviteUrl(Permission.ADMINISTRATOR)), true);
        eb.addField("Support Server", MarkdownUtil.maskedLink("Here", Constants.SUPPORT_SERVER), true);

        event.getChannel().sendMessage(eb.build()).queue();
    }

    @Override
    public String getHelp() {
        return "Gets invite links for the bot";
    }

    @Override
    public String getInvoke() {
        return "invite";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, this.getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "help";
    }
}

