package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.objects.ResponseCode;
import com.gitlab.badstagram.vortex.utils.Config;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import com.gitlab.badstagram.vortex.utils.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.HttpException;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.event.Level;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class News implements ICommand {

    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        try {
            EmbedBuilder eb = new EmbedBuilder();

            eb.setTitle("News");

            JSONArray news = getNews();

            Utils.log(this.getClass(), news, Level.DEBUG);

            for (int i = 0; i < news.length(); i++) {
                JSONObject article = news.getJSONObject(i);

                String title = article.getString("title");
                String description = article.optString("description", "None");
                String url = article.getString("url");
                int total = title.length() + description.length();

                if (total >= MessageEmbed.EMBED_MAX_LENGTH_BOT) {
                    break;
                }
                eb.addField(title, MarkdownUtil.maskedLink(description, url), false);
            }

            channel.sendMessage(eb.build()).queue();

        } catch (Exception e) {
            ErrorHandler.handleException(e, event, true);
        }
// [\[+\d chars\]]
    }

    JSONArray getNews() throws Exception {

        OkHttpClient client = Vortex.getHttpClient();
        Request request = new Request.Builder()
                .url(new URL("http://newsapi.org/v2/top-headlines?country=gb"))
                .addHeader("X-Api-Key", Config.getObject("tokens").getString("newsApi")).build();

        Response response = client.newCall(request).execute();


        if (response.code() == ResponseCode.OK){
            return new JSONObject(response.body().string()).getJSONArray("articles");
        }else {
            throw new HttpException("Received non-200 response code: "+response.code());
        }

    }


    @Override
    public String getHelp() {
        return "Gets the latest UK news";
    }

    @Override
    public String getInvoke() {
        return "news";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[0];
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}

