package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class ForceBan implements ICommand {

    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild,
                       User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }
        String reason = String.join(" ", args.subList(1, args.size()));
        long userId = 0L;

        try {
            userId = Long.parseLong(args.get(0));
        } catch (NumberFormatException e) {
            channel.sendMessage("User ID was not in the correct format.").queue();
        }

        jda.retrieveUserById(userId).submit()
                .thenAcceptAsync(user -> guild.ban(user, 7).reason("[Force Ban] " + reason).submit()
                        .thenAcceptAsync(v -> channel.sendMessageFormat(":white_check_mark: %s (`%s`) has been banned: `%s`", user.getAsTag(), user.getId(), reason).queue()))
                .exceptionally(thr -> {
                            ErrorHandler.handleException(thr, event, false);
                            return null;
                });
    }

    @Override
    public String getHelp() {

        return "Bans a user by an ID";
    }

    @Override
    public String getInvoke() {

        return "forceban";
    }

    @Override
    public String getUsage() {

        return String.format("%s%s <user ID>", Constants.PREFIX, getInvoke());
    }

    @Override
    public String getCategory() {

        return "moderation";
    }

    @Override
    public Permission[] getBotPermissions() {

        return new Permission[]{Permission.BAN_MEMBERS};
    }

    @Override
    public Permission[] getUserPermissions() {

        return new Permission[]{Permission.BAN_MEMBERS};
    }
}
