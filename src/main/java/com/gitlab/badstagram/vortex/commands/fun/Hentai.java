package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.TaggedImage;
import net.explodingbush.ksoftapi.enums.ImageTag;

import java.util.List;

public class Hentai implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        if (!event.getChannel().isNSFW()) {
            event.getChannel().sendMessage("This command can only be used in NSFW channels.").queue();
            return;
        }

        KSoftAPI api = Vortex.getKsoftApi();
        TaggedImage image = api.getTaggedImage(ImageTag.HENTAI_GIF).allowNsfw(true).execute();

        EmbedBuilder eb = new EmbedBuilder();
        eb.setImage(image.getUrl());
        event.getChannel().sendMessage(eb.build()).queue();
    }

    @Override
    public String getHelp() {
        return "NSFW: gets a hentai gif";
    }

    @Override
    public String getInvoke() {
        return "hentai";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}

