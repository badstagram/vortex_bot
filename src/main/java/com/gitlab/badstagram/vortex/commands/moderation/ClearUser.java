package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

public class ClearUser implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
//                          0          1
        //clearuser 696978475692589126 4

        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }
        int amount;

        try {
            if (args.size() == 2) {
                amount = Integer.parseInt(args.get(1));
            } else {
                amount = 100;
            }


            if (amount < 2 || amount > 100) {
                channel.sendMessage("Input must be between 2 and 100.").queue();
                return;
            }
            long userId = Long.parseLong(args.get(0));
            channel.getIterableHistory()
                    .takeAsync(amount)
                    .thenApplyAsync(messages -> {
                        List<Message> goodMessages = messages.stream()
                                .filter(m -> m.getTimeCreated().isBefore(OffsetDateTime.now().plus(2, ChronoUnit.WEEKS)) && m.getAuthor().getIdLong() == userId).collect(Collectors.toList());

                        channel.purgeMessages(goodMessages);
                        return goodMessages.size();
                    })
                    .whenCompleteAsync((count, thr) -> {
                        EmbedBuilder eb = new EmbedBuilder();
                        eb.setTitle("User Cleared");
                        eb.setDescription(String.format("**%d** messages by <@%s> in <#%s> was cleared by %s", count, userId, channel.getId(), event.getAuthor().getAsMention()));
                        eb.setColor(new Color(78, 93, 148));
                        channel.sendMessage(eb.build()).queue();
                    })
                    .exceptionally(thr -> {
                        if (thr != null) {
                            channel.sendMessage(thr.getMessage()).queue();
                        }
                        return 0;
                    });

        } catch (NumberFormatException ignored) {
            channel.sendMessage("Input was not a valid number.").queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }

    }

    @Override
    public String getHelp() {
        return "clears messages by a user";
    }

    @Override
    public String getInvoke() {
        return "clearuser";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <user id> [amount]", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_MANAGE};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{Permission.MESSAGE_MANAGE};
    }

    @Override
    public String[] getAliases() {
        return new String[]{"purgeuser"};
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

