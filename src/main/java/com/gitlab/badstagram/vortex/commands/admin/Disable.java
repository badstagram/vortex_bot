package com.gitlab.badstagram.vortex.commands.admin;

import com.gitlab.badstagram.vortex.CommandManager;
import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class Disable implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        try {
            ICommand command = CommandManager.getCommandByName(args.get(0));
            if (command != null) {
                CommandManager.disableCommand(command);
                channel.sendMessageFormat("Disabled `%s`", command.getInvoke()).queue();
            } else {
                channel.sendMessageFormat("Command `%s` not found", args.get(0)).queue();
            }
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Owner command: disabled a command globally";
    }

    @Override
    public String getInvoke() {
        return "disable";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <command>", Constants.PREFIX, getInvoke());
    }


    @Override
    public String getCategory() {
        return "admin";
    }

    @Override
    public boolean ownerCommand() {
        return true;
    }
}
