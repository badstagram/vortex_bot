package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.explodingbush.ksoftapi.KSoftAPI;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class SuperBan implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        //              0          1        2
        // v!superban <user id> <proof> <reason>
        KSoftAPI ksoft = Vortex.getKsoftApi();
        User moderator = event.getAuthor();
        List<String> argsPipe = Arrays.asList(String.join(" ", args).split(" \\| "));
        if (argsPipe.size() < 3) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("There was an error running that command!");
            eb.setDescription("`Invalid syntax`");
            eb.setFooter(String.format("%s | For support join out support guild: %s", getUsage(), Constants.SUPPORT_SERVER));

            event.getChannel().sendMessage(eb.build()).queue();
            return;
        }
        event.getJDA().retrieveUserById(args.get(0)).submit().whenComplete(((user, throwable) -> {
            if (throwable != null) {
                event.getChannel().sendMessage(throwable.getMessage().replaceAll("\\d{5}:", "")).queue();
            }

            String offenderId = user.getId();
            String offenderName = user.getName();
            String offenderDiscrim = user.getDiscriminator();

            String modId = moderator.getId();

            String reason = String.join(" ", args.subList(2, args.size()));
            
            Pattern imgurImage = Pattern.compile("^https?://i\\.imgur\\.com/(\\w+)\\.(?:jpg|png)$");
            Pattern imgurAlbum = Pattern.compile("^https?://imgur\\.com/a/(\\w+)$");

            if (!imgurImage.matcher(args.get(1)).find() || !imgurAlbum.matcher(args.get(1)).find()) {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle("There was an error running that command!");
                eb.setDescription("`Invalid syntax`");
                eb.setFooter(String.format("%s | For support join out support guild: %s", getUsage(), Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();
                return;
            }

            ksoft.getBan().addBan()
                    .setUserId(offenderId)
                    .setUsername(offenderName)
                    .setDiscriminator(offenderDiscrim)
                    .setProof(args.get(1))
                    .setReason(reason)
                    .setModeratorId(modId)
                    .set().execute();
        }));

        event.getChannel().sendMessage("KSoft.Si global ban reported!").queue();
    }

    @Override
    public String getHelp() {
        return "Reports a ban to KSoft.Si";
    }

    @Override
    public String getInvoke() {
        return "superban";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <user id> <Imgur url> <reason>", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

