package com.gitlab.badstagram.vortex.commands.info;

import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.*;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;
import java.util.stream.Collectors;

public class GuildInfo implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        guild.retrieveOwner().submit()
                .thenComposeAsync(owner -> {
                    guild.retrieveBanList().submit()
                            .thenComposeAsync(bans -> {
                                List<Member> members = guild.getMembers();
                                List<Member> onlineMembers = members.stream()
                                        .filter(m -> !m.getOnlineStatus().equals(OnlineStatus.OFFLINE))
                                        .collect(Collectors.toList());
                                List<Member> offlineMembers = members.stream()
                                        .filter(m -> m.getOnlineStatus().equals(OnlineStatus.OFFLINE))
                                        .collect(Collectors.toList());

                                long botCount = members.stream()
                                        .map(Member::getUser)
                                        .filter(User::isBot)
                                        .count();
                                long humanCount = members.stream()
                                        .map(Member::getUser)
                                        .filter(u -> !u.isBot())
                                        .count();

                                Region region = guild.getRegion();
                                MessageEmbed embed =
                                        new EmbedBuilder()
                                                .setTitle(guild.getName())
                                                .addField("ID", guild.getId(), true)
                                                .addField("Owner", owner.getAsMention(), true)
                                                .addField("Members", String.format("%d Online | %d Offline | %d Bots | %d Humans | %d Total", onlineMembers.size(), offlineMembers.size(), botCount, humanCount, members.size()), false)
                                                .addField("Channels", String.format("%d Text | %d Voice", guild.getTextChannels().size(), guild.getVoiceChannels().size()), false)
                                                .addField("Roles", String.valueOf(guild.getRoles().size()), true)
                                                .addField("Emotes", String.valueOf(guild.getEmotes().size()), true)
                                                .addField("Region", String.format("%s %s", region.getEmoji(), region.getName()), false)
                                                .addField("Ban Count", String.valueOf(bans.size()), false)
                                                .build();

                                channel.sendMessage(embed).queue();
                                return null;
                            });
                    return null;
                });
    }

    @Override
    public String getHelp() {
        return "gets info about the guild";
    }

    @Override
    public String getInvoke() {
        return "guildinfo";
    }

    @Override
    public String getCategory() {
        return "info";
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }
}
