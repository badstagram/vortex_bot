package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class AddRole implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        if (args.size() < 2) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }

        if (mentionedMembers.isEmpty()) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }

        Member target = mentionedMembers.get(0);
        String roleName = String.join(" ", args.subList(1, args.size()));
        List<Role> roles = guild.getRolesByName(roleName, true);
        if (roles.isEmpty()) {
            channel.sendMessage("No roles found with that name.").queue();
            return;
        }
        Role role = roles.get(0);

        if (!selfMember.canInteract(role)){
            channel.sendMessageFormat("Could not remove the `%s` role from %s", role.getName(), target.getAsMention()).queue();
            return;
        }
        guild.addRoleToMember(target, role).queue(ignored -> channel.sendMessageFormat("Added the `%s` role to %s", roleName, target.getAsMention()).queue(), thr -> ErrorHandler.handleException(thr, event, false));

    }

    @Override
    public String getHelp() {
        return "Adds a role to a member.";
    }

    @Override
    public String getInvoke() {
        return "addrole";
    }

    @Override
    public String getUsage() {
        return "%s%s <@user> <role name>";
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MANAGE_ROLES};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{Permission.MANAGE_ROLES};
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}
