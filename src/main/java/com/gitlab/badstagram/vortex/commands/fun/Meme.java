package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Reddit;
import net.explodingbush.ksoftapi.enums.ImageType;

import java.util.List;

public class Meme implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        KSoftAPI kSoftAPI = Vortex.getKsoftApi();
        Reddit meme = kSoftAPI.getRedditImage(ImageType.RANDOM_MEME).execute();
        EmbedBuilder eb = new EmbedBuilder();
        eb.setAuthor(meme.getTitle(), meme.getSourceUrl());
        eb.setImage(meme.getImageUrl());
        eb.setFooter(String.format("upvotes: %d | Subreddit: %s | Posted by %s", meme.getUpvotesInt(), meme.getSubreddit(), meme.getAuthor()));
        event.getChannel().sendMessage(eb.build()).queue();
    }

    @Override
    public String getHelp() {
        return "Sends a meme";

    }

    @Override
    public String getInvoke() {
        return "meme";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}

