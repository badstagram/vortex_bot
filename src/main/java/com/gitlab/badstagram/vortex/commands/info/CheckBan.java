package com.gitlab.badstagram.vortex.commands.info;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.utils.MarkdownUtil;
import net.explodingbush.ksoftapi.KSoftAPI;
import net.explodingbush.ksoftapi.entities.Ban;

import java.awt.*;
import java.util.List;

public class CheckBan implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        try {
            EmbedBuilder eb = new EmbedBuilder();


            if (args.size() != 1) {
                eb.setTitle("There was an error running that command.");
                eb.setDescription("`Invalid syntax.`");
                eb.setFooter(String.format("Usage: %s | For support, join our support guild: %s", getUsage(), Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                eb.clear();
                return;
            }

            KSoftAPI api = Vortex.getKsoftApi();
            Ban globalBanInfo = api.getBan().setUserId(args.get(0)).execute();

            jda.retrieveUserById(globalBanInfo.getModId()).submit().thenAccept(mod -> {
                if (!globalBanInfo.exists()) {
                    eb.setColor(new Color(0, 255, 0));
                    eb.setDescription("User is not global banned.");
                } else {
                    eb.setColor(new Color(255, 0, 0));
                    eb.setTitle("User is global banned.");
                    eb.addField("Ban Reason", globalBanInfo.getReason(), false);
                    eb.addField("Proof", MarkdownUtil.maskedLink("Here", globalBanInfo.getProof()), false);
                    eb.addField("Active", String.valueOf(globalBanInfo.isBanActive()), false);

                    eb.addField("Moderator", mod.getAsTag(), false);

                }
                eb.setFooter("Powered By KSoft.Si Bans.");
                event.getChannel().sendMessage(eb.build()).queue();
            });

            eb.setFooter("Powered By KSoft.Si Bans.");
            event.getChannel().sendMessage(eb.build()).queue();

        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }


    }

    @Override
    public String getHelp() {
        return "Checks if a user is banned on KSoft.Si.";
    }

    @Override
    public String getInvoke() {
        return "checkban";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <user id>", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[0];
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

