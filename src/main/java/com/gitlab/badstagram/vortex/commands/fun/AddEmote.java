package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.internal.requests.Requester;
import okhttp3.Request;
import okhttp3.Response;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddEmote implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        try {
            List<String> argus = Arrays.asList(String.join(" ", args).split(" \\| "));
            EmbedBuilder eb = new EmbedBuilder();
            if (argus.size() > 2) {
                eb.setTitle("There was an error running that command.");
                eb.setDescription("`Invalid syntax.`");
                eb.setFooter(String.format("Usage: %s | For support, join our support guild: %s", getUsage(), Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                eb.clear();
                return;
            }

            String name = argus.get(0);
            String url = argus.get(1).replace("<", "").replace(">", "");
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("User-Agent", Requester.USER_AGENT).build();
            Response response = Vortex.getHttpClient().newCall(request).execute();

            Icon imageIcon = Icon.from(response.body().bytes());

            if (guild.getEmotes().size() == guild.getMaxEmotes()) {
                event.getChannel().sendMessage(String.format("This guild already has the max emotes allowed (%d)!", guild.getMaxEmotes())).queue(m -> m.delete().queueAfter(1, TimeUnit.MINUTES));
                return;
            }

            if (!guild.getEmotesByName(name, true).isEmpty()) {
                event.getChannel().sendMessage("This guild already has an emote with that name!").queue(m -> m.delete().queueAfter(1, TimeUnit.MINUTES));
                return;
            }

            guild.createEmote(name, imageIcon).queue(emote -> {
                eb.setTitle("Emote Added");
                eb.addField("Name", emote.getName(), true);
                eb.addField("ID", emote.getId(), true);
                eb.addField("Animated", emote.isAnimated() ? "Yes" : "No", true);
                eb.addField("Preview", emote.getAsMention(), true);
                eb.addField("Amount Of Emotes",
                        String.valueOf(guild.getEmotes().size()),
                        true);
                event.getChannel().sendMessage(eb.build()).queue();

            }, err -> {
                eb.setTitle("There was an error running that command.");
                eb.setDescription(String.format("`%s`", err.getMessage()));
                eb.setFooter(String.format("Usage: %s | For support, join our support guild: %s", getUsage(), Constants.SUPPORT_SERVER));

                event.getChannel().sendMessage(eb.build()).queue();

                eb.clear();
            });

        } catch (MalformedURLException e) {

            event.getChannel().sendMessage("The provided url was not valid!").queue(m -> m.delete().queueAfter(1, TimeUnit.MINUTES));


        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Adds an emote to a guild";
    }

    @Override
    public String getInvoke() {
        return "addemote";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <name> | <image url>", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS, Permission.MANAGE_EMOTES};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{Permission.MANAGE_EMOTES};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}
