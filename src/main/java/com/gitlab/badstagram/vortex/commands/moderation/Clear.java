package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

public class Clear implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        try {
            int messagesToClear;
            if (args.isEmpty()) {
                messagesToClear = 100;
            } else {
                messagesToClear = Integer.parseInt(args.get(0)) + 1;
            }

            if (messagesToClear < 2 || messagesToClear > 100) {
                channel.sendMessage("Input must be between 2 and 100.").queue();
                return;
            }


            channel.getIterableHistory()
                    .takeAsync(messagesToClear)
                    .thenApplyAsync(messages -> {
                        List<Message> goodMessages = messages.stream()
                                .filter(m -> m.getTimeCreated().isBefore(OffsetDateTime.now().plus(2, ChronoUnit.WEEKS))).collect(Collectors.toList());

                        channel.purgeMessages(goodMessages);
                        return goodMessages.size();
                    })
                    .whenCompleteAsync((count, thr) -> {
                        EmbedBuilder eb = new EmbedBuilder();
                        eb.setTitle("Channel Cleared");
                        eb.setDescription(String.format("**%d** messages was cleared by %s", count, event.getAuthor().getAsMention()));
                        eb.setColor(new Color(78, 93, 148));
                        channel.sendMessage(eb.build()).queue();
                    })
                    .exceptionally(thr -> {
                        if (thr != null) {
                            channel.sendMessage(thr.getMessage()).queue();
                        }
                        return 0;
                    });


        } catch (NumberFormatException ignored) {
            channel.sendMessage("Input was not a valid number.").queue();
        } catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Clears messages from a channel";
    }

    @Override
    public String getInvoke() {
        return "clear";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s [amount]", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_MANAGE};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{Permission.MESSAGE_MANAGE};
    }

    @Override
    public String[] getAliases() {
        return new String[]{"purge"};
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

