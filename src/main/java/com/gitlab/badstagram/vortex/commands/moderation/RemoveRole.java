package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class RemoveRole implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        if (args.size() < 2) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }

        if (mentionedMembers.isEmpty()) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }

        Member target = mentionedMembers.get(0);
        String roleName = String.join(" ", args.subList(1, args.size()));
        List<Role> roles = guild.getRolesByName(roleName, true);
        if (roles.isEmpty()) {
            channel.sendMessage("No roles found with that name.").queue();
            return;
        }
        Role role = roles.get(0);


        guild.removeRoleFromMember(target, role).queue(ignored -> channel.sendMessageFormat("Removed the `%s` role from %s", roleName, target.getAsMention()).queue(), thr -> ErrorHandler.handleException(thr, event, false));
    }

    @Override
    public String getHelp() {
        return "removes a role from a member";
    }

    @Override
    public String getInvoke() {
        return "removerole";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <role>", Constants.PREFIX, getInvoke());
    }


    @Override
    public String getCategory() {
        return null;
    }
}
