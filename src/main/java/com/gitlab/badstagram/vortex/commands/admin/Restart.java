package com.gitlab.badstagram.vortex.commands.admin;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessage;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.Config;
import com.gitlab.badstagram.vortex.utils.Utils;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.io.File;
import java.util.List;

public class Restart implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        event.getChannel().sendMessage("Now updating... Please wait...").queue();
        try {
            event.getJDA().shutdown();
            new ProcessBuilder()
                    .directory(new File("/root/Vortex_Bot"))
                    .command("systemctl", "restart", "vortex.service").start();

        } catch (Exception e) {
            WebhookEmbedBuilder builder = new WebhookEmbedBuilder();
            builder.setTitle(new WebhookEmbed.EmbedTitle("Error while restarting!", null));
            builder.setColor(0xFF0000);
            builder.setDescription(e.getMessage());

            WebhookMessageBuilder messageBuilder = new WebhookMessageBuilder();
            WebhookMessage message = messageBuilder
                    .setContent("<@424239181296959507>")
                    .addEmbeds(builder.build()).build();

            WebhookClient client = WebhookClient.withUrl(Config.getString("statusLog"));

            client.send(message).thenAccept(ignored -> client.close());
        }
    }

    @Override
    public String getHelp() {
        return "Owner command: Restarts the bot.";
    }

    @Override
    public String getInvoke() {
        return "restart";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "admin";
    }

    @Override
    public boolean ownerCommand() {
        return true;
    }
}
