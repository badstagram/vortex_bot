package com.gitlab.badstagram.vortex.commands.fun;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.main.Vortex;
import com.gitlab.badstagram.vortex.objects.ICommand;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class AFK implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        if (!selfMember.canInteract(member)) {
            channel.sendMessage("I am unable to change your nickname.").queue();
            return;
        }

        String newNickname = "[AFK] ";
        if (!args.isEmpty()) {
            newNickname += String.join(" ", args);
        } else {
            newNickname += author.getName();
        }

        member.modifyNickname(newNickname).queue(ignored -> channel.sendMessage("I've marked you as AFK. See you soon!").queue());
        Vortex.getAfkUsers().add(member.getIdLong());
    }

    @Override
    public String getHelp() {
        return "Marks you as AFK with an optional reason";
    }

    @Override
    public String getInvoke() {
        return "afk";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s [reason]", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.NICKNAME_MANAGE};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[0];
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "fun";
    }
}
