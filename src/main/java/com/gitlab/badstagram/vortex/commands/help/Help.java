package com.gitlab.badstagram.vortex.commands.help;

import com.gitlab.badstagram.vortex.CommandManager;
import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.Enums.Module;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class Help implements ICommand {
    private Map<String, ICommand> commands;

    public Help(Map<String, ICommand> commands) {
        this.commands = commands;
    }

    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {
        if (args.isEmpty()) {
            EmbedBuilder eb = new EmbedBuilder();
            StringBuilder descriptionBuilder = eb.getDescriptionBuilder();
            eb.setTitle("Help");
            descriptionBuilder.append("Vortex currently has the following modules available```").append("\n");
            descriptionBuilder.append("Admin\n");
            descriptionBuilder.append("Fun\n");
            descriptionBuilder.append("Help\n");
            descriptionBuilder.append("Info\n");
            descriptionBuilder.append("Moderation```\n");

            eb.setDescription(descriptionBuilder.toString());
            eb.setFooter(String.format("Type %shelp <module> for a list of that modules commands", Constants.PREFIX));
            event.getChannel().sendMessage(eb.build()).queue();
            return;
        }

        String module = args.get(0).toLowerCase(Locale.UK);
        switch (module) {
            case "admin":
                displayHelp(Module.ADMIN, event.getChannel());
                break;
            case "fun":
                displayHelp(Module.FUN, event.getChannel());
                break;
            case "help":
                displayHelp(Module.HELP, event.getChannel());
                break;
            case "info":
                displayHelp(Module.INFO, event.getChannel());
                break;
            case "moderation":
                displayHelp(Module.MODERATION, event.getChannel());
                break;
            default:
                event.getChannel().sendMessage("Unknown module").queue();
                break;
        }
    }


    private void displayHelp(Module module, TextChannel channel) {
        EmbedBuilder eb = new EmbedBuilder();
        List<ICommand> cmds = CommandManager.getAsList();
        switch (module) {
            case ADMIN:
                List<ICommand> adminCommands = cmds.stream()
                        .filter(cmd -> cmd.getCategory().equalsIgnoreCase("admin"))
                        .collect(Collectors.toList());

                if (!adminCommands.isEmpty()) {
                    for (ICommand command : adminCommands) {
                        eb.addField(command.getInvoke(), String.format(
                                        "Help: `%s`\n" +
                                        "Usage: `%s`\n" +
                                        "Bot Permissions: `%s`\n" +
                                        "User Permissions: `%s`\n",
                                command.getHelp(),
                                command.getUsage(),
                                CommandManager.getBotPermissions(command),
                                CommandManager.getUserPermissions(command)),
                                false);
                    }
                }
                channel.sendMessage(eb.build()).queue();
                break;

            case INFO:
                List<ICommand> infoCommands = cmds.stream()
                        .filter(cmd -> cmd.getCategory().equalsIgnoreCase("info"))
                        .collect(Collectors.toList());

                if (!infoCommands.isEmpty()) {
                    for (ICommand command : infoCommands) {
                        eb.addField(command.getInvoke(), String.format(
                                        "Help: `%s`\n" +
                                        "Usage: `%s`\n" +
                                        "Bot Permissions: `%s`\n" +
                                        "User Permissions: `%s`\n",
                                command.getHelp(),
                                command.getUsage(),
                                CommandManager.getBotPermissions(command),
                                CommandManager.getUserPermissions(command)),
                                false);
                    }
                }
                channel.sendMessage(eb.build()).queue();
                break;
            case FUN:
                List<ICommand> funCommands = cmds.stream()
                        .filter(cmd -> cmd.getCategory().equalsIgnoreCase("fun"))
                        .collect(Collectors.toList());

                if (!funCommands.isEmpty()) {
                    for (ICommand command : funCommands) {
                        eb.addField(command.getInvoke(), String.format(
                                        "Help: `%s`\n" +
                                        "Usage: `%s`\n" +
                                        "Bot Permissions: `%s`\n" +
                                        "User Permissions: `%s`\n",
                                command.getHelp(),
                                command.getUsage(),
                                CommandManager.getBotPermissions(command),
                                CommandManager.getUserPermissions(command)),
                                false);
                    }
                }
                channel.sendMessage(eb.build()).queue();
                break;
            case HELP:
                List<ICommand> helpCommands = cmds.stream()
                        .filter(cmd -> cmd.getCategory().equalsIgnoreCase("help"))
                        .collect(Collectors.toList());

                if (!helpCommands.isEmpty()) {
                    for (ICommand command : helpCommands) {
                        eb.addField(command.getInvoke(), String.format(
                                        "Help: `%s`\n" +
                                        "Usage: `%s`\n" +
                                        "Bot Permissions: `%s`\n" +
                                        "User Permissions: `%s`\n",
                                command.getHelp(),
                                command.getUsage(),
                                CommandManager.getBotPermissions(command),
                                CommandManager.getUserPermissions(command)),
                                false);
                    }
                }
                channel.sendMessage(eb.build()).queue();
                break;
            case MODERATION:
                List<ICommand> moderationCommands = cmds.stream()
                        .filter(cmd -> cmd.getCategory().equalsIgnoreCase("moderation"))
                        .collect(Collectors.toList());

                if (!moderationCommands.isEmpty()) {
                    for (ICommand command : moderationCommands) {
                        eb.addField(command.getInvoke(), String.format(
                                        "Help: %s\n" +
                                        "Usage: %s\n" +
                                        "Bot Permissions: %s\n" +
                                        "User Permissions: %s\n",
                                command.getHelp(),
                                command.getUsage(),
                                CommandManager.getBotPermissions(command),
                                CommandManager.getUserPermissions(command)),
                                false);
                    }
                }
                channel.sendMessage(eb.build()).queue();
                break;
        }
    }


    @Override
    public String getHelp() {
        return null;
    }

    @Override
    public String getInvoke() {
        return "help";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ADD_REACTION};
    }

    @Override
    public Permission[] getUserPermissions() {
        return Permission.EMPTY_PERMISSIONS;
    }

    @Override
    public String[] getAliases() {
        return new String[] {"test"};
    }

    @Override
    public String getCategory() {
        return "help";
    }
}

