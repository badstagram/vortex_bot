package com.gitlab.badstagram.vortex.commands.moderation;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.ErrorHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class ClearUntil implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {

        if (args.isEmpty()) {
            ErrorHandler.invalidSyntax(event, this);
            return;
        }
        try {
            long messageId = Long.parseLong(args.get(0));
            channel.getHistoryAfter(messageId, 100).submit()
                    .thenApplyAsync(history -> {
                        List<Message> messages = history.getRetrievedHistory();
                        channel.purgeMessages(messages);
                        return null;
                    })
                    .exceptionally(thr -> {
                        if (thr != null) {
                            channel.sendMessage(thr.getMessage()).queue();
                        }
                        return 0;
                    });

        } catch (NumberFormatException e) {
            channel.sendMessage("Input was not a valid number.").queue();
        }
        catch (Exception e) {
            ErrorHandler.handleException(e, event, false);
        }
    }

    @Override
    public String getHelp() {
        return "Clears messages up to the provided message ID.";
    }

    @Override
    public String getInvoke() {
        return "clearuntil";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s <message ID>", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[] {Permission.MESSAGE_MANAGE};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[] {Permission.MESSAGE_MANAGE};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "moderation";
    }
}

