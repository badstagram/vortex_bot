package com.gitlab.badstagram.vortex.commands.info;

import com.gitlab.badstagram.vortex.Constants;
import com.gitlab.badstagram.vortex.objects.ICommand;
import com.gitlab.badstagram.vortex.utils.Utils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

public class UserInfo implements ICommand {
    @Override
    public void handle(List<String> args, GuildMessageReceivedEvent event, JDA jda, TextChannel channel, Guild guild, User author, Member member, Member selfMember, SelfUser selfUser, List<Member> mentionedMembers) {


        User user;
        Member m;
        EmbedBuilder eb = new EmbedBuilder();
        if (mentionedMembers.isEmpty()) {
            m = member;
        } else {
            m = mentionedMembers.get(0);
        }
        user = m.getUser();

        boolean bot = user.isBot();
        List<Role> roleList = m.getRoles();
        EnumSet<User.UserFlag> flags = m.getUser().getFlags();
        EnumSet<Permission> permissions = m.getPermissions();
        String roles = roleList.stream()
                .map(Role::getAsMention)
                .collect(Collectors.joining(", "));

        OffsetDateTime createdTime = user.getTimeCreated();
        OffsetDateTime joinedTime = m.getTimeJoined();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd:MM:yyyy KK:mm a");
        eb.setAuthor(user.getAsTag(), null, user.getAvatarUrl());
        eb.addField("Mention (id)", String.format("%s (%s) %s", user.getAsMention(), user.getId(), bot ? Constants.BOT_EMOTE : ""), false);
        eb.addField("Time Account Created", formatter.format(createdTime), false);
        eb.addField("Time Member Joined", formatter.format(joinedTime), false);

        StringBuilder sb = new StringBuilder();
        if (!flags.isEmpty()) {
            sb.append(flags.stream()
                    .map(User.UserFlag::getName)
                    .collect(Collectors.joining(", ")));
        } else {
            sb.append("None");
        }

        StringBuilder sb2 = new StringBuilder();
        if (!permissions.isEmpty()) {
            sb2.append(permissions.stream()
                    .map(Permission::getName)
                    .collect(Collectors.joining(", ")));
        } else {
            sb.append("None");
        }
        eb.addField("Flags", Utils.replaceLast(sb.toString(), ", ", ""), false);
        eb.addField("Permissions", m.isOwner() ? "Server Owner" : sb2.toString(), false);
        eb.addField("Roles", roleList.isEmpty() ? "None" : roles, false);

        event.getChannel().sendMessage(eb.build()).queue();
    }


    @Override
    public String getHelp() {
        return "Gets info about a user.";
    }

    @Override
    public String getInvoke() {
        return "userinfo";
    }

    @Override
    public String getUsage() {
        return String.format("%s%s [@member]", Constants.PREFIX, getInvoke());
    }

    @Override
    public Permission[] getBotPermissions() {
        return new Permission[]{Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_EXT_EMOJI};
    }

    @Override
    public Permission[] getUserPermissions() {
        return new Permission[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getCategory() {
        return "info";
    }
}

